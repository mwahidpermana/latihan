<?php
function tanggal($tanggal)
{
    $bulan = array(
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember',
    );

    $pecah = explode('-', $tanggal);

    return $bulan[(int)$pecah[1]].' '.$pecah[0];
}

$date="2011-04-01";
$banyak = "12";
$no = 0;

$start = strtotime($date);
$end = strtotime(date('Y-m-d', strtotime($date. ' + '.$banyak.' months')));

echo "<table border='1'>";
while($start < $end)
{
    $bulan = date('Y-m-d', $start);
    if ($no % 3 == 0) {
        echo '<tr>';
    }
    echo '<td><input type="checkbox" name="bulan_ke[]" value"'.tanggal($bulan).'">'.tanggal($bulan).'</td>';
    if ($no % 3 == 3) {
        echo '</tr>';
    }
    $start = strtotime("+1 month", $start);
    $no++;
}
echo "</table>";

?>